package info1Classes;

public class Physics {

	private static final float gravity = (float) 9.80665;
	private static final int c = 299792458;
	
	public static float getGravity() {
		return gravity;
	}
	
	public static int getSpeedOfLight() {
		return c;
	}
	
}
