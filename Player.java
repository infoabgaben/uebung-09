package info1Classes;

import java.util.logging.Logger;

import info1Classes.Physics; // unn�tigster Import in der Geschichte von Java :D

public class Player {

	private String name;
	private float[] scores;
	
	public float mass;
	
	public Player() {
		name = "default";
		scores = new float[0];
		mass = 75;
	}
	
	public Player(String name, float[] scores, float mass) {
		this.name = name;
		this.scores = scores;
		this.mass = mass;
	}
	
	public Player(Player p) {
		name = p.name;
		scores = p.scores;
		mass = p.mass;
	}
	
	private float getWeight() {
		return Physics.getGravity() * mass;
	}
	
	public void showPoints() {
		if(scores.length > 0) {
			System.out.print(name + "'s previous scores are: " + scores[0]);
			for(int i = 1; i < scores.length; i++) {
				System.out.print(", " + scores[i]);
			}
			System.out.print("!\n");
		
		}else {
			String warning = "The Player does not have previously played!";
			
			Logger LOGGER = Logger.getLogger(Player.class.getName());
			LOGGER.warning(warning); //ich wei� das war so nicht gemeint, deswegen hier noch die erwartete L�sung :D
			
//			System.out.println("WARNING" + warning);
		}
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setPoints(float[] scores) {
		this.scores = scores;
	}
	
	public float[] getPoints() {
		return scores;
	}
	
	public float massDiff(Player p) {
		return Math.abs(mass - p.mass);
	}
	
	public float[] pointDiff(Player p) {
		float[] result = new float[Math.min(scores.length, p.scores.length)];
		for(int i = 0; i < scores.length && i < p.scores.length; i++)
			result[i] = Math.abs(scores[i] - p.scores[i]);
		
		return result;
	}
}
